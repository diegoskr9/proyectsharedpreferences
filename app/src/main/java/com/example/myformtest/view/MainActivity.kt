package com.example.myformtest.view

import android.os.Bundle
import android.view.View
import com.example.myformtest.R
import com.example.myformtest.util.Constants
import com.example.myformtest.util.MyApp

class MainActivity : MyApp() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


    }

    fun onClick(view:View){

        when(view.id){

            R.id.buttonCreateProfile -> {
                goCreateProfile()
            }
            R.id.buttonViewProfile -> {
                searchUser()
            }
            R.id.buttonDeletePref -> {
                //goDeleteProfile()
                deleteUser()
            }

        }

    }


    private fun goCreateProfile(){

        myIntents(CreateProfileActivity::class.java)

    }


    private fun searchUser(){

        myIntents(FindUserActivity::class.java)

    }

    private fun deleteUser(){

        myIntents(DeleteProfileActivity::class.java)


    }



}
