package com.example.myformtest.view

import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.example.myformtest.R
import com.example.myformtest.util.Constants
import com.example.myformtest.util.MyApp
import kotlinx.android.synthetic.main.activity_delete_profile.*

class DeleteProfileActivity : MyApp() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delete_profile)


    }

    fun deleteUser(view:View){

        val preferenceManager = getMyPreferences(Constants.USER_PREF)

        val idUser = editTextUserDelete.text.toString()

        if(findUser(idUser,preferenceManager)){

            val builder = AlertDialog.Builder(this)
            builder.setTitle(R.string.confirmation)
                   .setMessage(R.string.deleteconfirmation)
                   .setPositiveButton(R.string.deleteid, DialogInterface.OnClickListener{dialog,id->
                       val editor = preferenceManager.edit()

                       editor.remove(idUser)
                       editor.apply()

                       showToast(R.string.userdeleteok)
                   }).setNegativeButton(R.string.cancel, DialogInterface.OnClickListener{dialog,id->

                      showToast(R.string.processcanceled)

                })

            var dialog = builder.create()
            dialog.show()

        }else{
            showToast(R.string.notuserfind)
        }

    }

}
