package com.example.myformtest.view

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.example.myformtest.R
import com.example.myformtest.model.Users
import com.example.myformtest.util.Constants
import com.example.myformtest.util.MyApp
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_view_profile.*

class ViewProfileActivity : MyApp() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_profile)

        showPrefsUser()


    }


    private fun showPrefsUser(){


        val preferenceManager = getMyPreferences(Constants.USER_PREF)

        val message = intent.getStringExtra(Constants.USER_ID)

        val userPref = preferenceManager.getString(message,"")

        val myUser = Gson().fromJson(userPref,Users::class.java)


        textViewNameR.text = myUser.userNames
        textViewEmailR.text =  myUser.userEmail
        textViewAddressR.text = myUser.userAddress
        textViewAgeR.text = myUser.userAge.toString()
        textViewGenderR.text = myUser.userGender
        textViewPassR.text = myUser.userPass

        /*textViewNameR.text =  preferenceManager.getString("prefNAME","No existe valor ")
        textViewEmailR.text = preferenceManager.getString("prefEMAIL","No existe valor")
        textViewAddressR.text = preferenceManager.getString("prefADDRESS","No existe valor")
        textViewAgeR.text = preferenceManager.getInt("prefAGE",0).toString()
        textViewGenderR.text = preferenceManager.getString("prefGENDER","No existe valor")
        textViewPassR.text = preferenceManager.getString("prefPASS","No existe valor")*/


    }

}
