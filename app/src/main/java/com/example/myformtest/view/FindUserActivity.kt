package com.example.myformtest.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import com.example.myformtest.util.Constants
import android.os.Bundle
import android.view.View
import com.example.myformtest.R
import com.example.myformtest.util.MyApp
import kotlinx.android.synthetic.main.activity_find_user.*

class FindUserActivity : MyApp() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_find_user)

    }


    fun findUser(view:View){

        val preferenceManager = getMyPreferences(Constants.USER_PREF)

        val userFind = editTextUserFind.text.toString()

        if(preferenceManager.getString(userFind,"")!=""){

            //Go viewProfile

            val myIntent = Intent(this,ViewProfileActivity::class.java).apply {

                putExtra(Constants.USER_ID,userFind)

            }

            startActivity(myIntent)

        }else{

            showToast(R.string.notuserfind)

        }


    }


}
