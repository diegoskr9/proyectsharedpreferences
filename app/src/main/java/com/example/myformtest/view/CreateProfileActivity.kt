package com.example.myformtest.view

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.Toast
import com.example.myformtest.R
import com.example.myformtest.model.Users
import com.example.myformtest.util.Constants
import com.example.myformtest.util.MyApp
import com.example.myformtest.util.Validations
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_create_profile.*

class CreateProfileActivity : MyApp() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_profile)

    }


    fun saveUser(view: View){


        val preferenceManager = getMyPreferences(Constants.USER_PREF)

        var userID:String?  = textInputEditUserId.text.toString()
        var name:String? =  textInputEditTextName.text.toString()
        var email:String? = textInputEditTextEmail.text.toString()
        var age:String? = textInputEditTextAge.text.toString()
        var address:String?= textInputEditTextAddrees.text.toString()
        var pass:String = textInputEditTextPass.text.toString()
        var gender = when {
            radioButton.isChecked -> {
                radioButton.text.toString()
            }
            radioButton2.isChecked -> {
                radioButton2.text.toString()
            }
            else -> {
                null
            }
        }

        val validate = Validations()

        if(!validate.camposEmpty(userID,name,email,age,address,pass,gender)){

            if(!findUser(userID,preferenceManager)){

                var cont = 0

                if(validate.validateID(userID)){
                    textInputEditUserId.error = "id number no greater than 10 digits"
                }else{
                    cont++
                }
                if(validate.validateLength(name)){//good
                    textInputEditTextName.error = "name too long"
                }else{
                    cont++
                }
                if(!validate.validateAge(age)){//good
                    textInputEditTextAge.error = "Please enter a valid age"
                }else{
                    cont++
                }
                if(!validate.validateEmail(email)){//good
                    textInputEditTextEmail.error = "invalid email"
                }else{
                    cont++
                }
                if(validate.validateLength(address)){
                    textInputEditTextAddrees.error = "address too long"
                }else{
                    cont++
                }
                if(validate.validateLength(pass)){
                    textInputEditTextPass.error = "enter a shorter password"
                }else{
                    cont++
                }
                if(cont==6){
                    var user = Users(userID,name,email,age,address,gender,pass)
                    addUser(userID,user,preferenceManager)
                }
            }else{
                showToast(R.string.userExists)
                textInputEditUserId.error = "Try another id"
            }

        }else{
            showToast(R.string.allrequired)
        }
    }

    private fun addUser(idUser:String?,user:Users,prefManager:SharedPreferences){

        val editor = prefManager.edit()

        editor.putString(idUser,Gson().toJson(user))

        editor.apply()

        showAlert(R.string.viewprofile,R.string.successfulUser)

        emptyInputs()


    }

    private fun emptyInputs(){

        textInputEditUserId.setText("")
        textInputEditTextName.setText("")
        textInputEditTextEmail.setText("")
        textInputEditTextAge.setText("")
        textInputEditTextAddrees.setText("")
        textInputEditTextPass.setText("")
        radioButton.isChecked = false
        radioButton2.isChecked= false


    }


}
