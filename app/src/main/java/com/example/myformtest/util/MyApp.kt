package com.example.myformtest.util

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.myformtest.R
import com.example.myformtest.model.Users

open class MyApp : AppCompatActivity(){

    private val usersList = mutableListOf<Users>()


    fun myIntents(myClass:Class<*>){
        //val myClassActivity = Class.forName(myClass)

        val intent = Intent(this,myClass)
        startActivity(intent)

    }

    fun getMyPreferences(myPref:String):SharedPreferences{

        return getSharedPreferences(myPref, Context.MODE_PRIVATE)

    }


    fun showAlert(tittle: Int, message: Int){

        var builder = AlertDialog.Builder(this)
        builder.setTitle(tittle).setMessage(message)
            .setPositiveButton(
                R.string.ok,
             DialogInterface.OnClickListener { dialog, id ->

        })
        var dialog = builder.create()
        dialog.show()


    }

    fun showToast(message: Int){

        Toast.makeText(this,message,Toast.LENGTH_SHORT).show()
    }

    fun findUser(idUser: String?, prefManager:SharedPreferences):Boolean{

        if(prefManager.getString(idUser,"")!=""){
            return true
        }
        return false

    }



}