package com.example.myformtest.util

import android.util.Patterns
import java.util.regex.Pattern

class Validations{

    fun camposEmpty(
        userID: String?,
        name: String?,
        email: String?,
        age: String?,
        address: String?,
        pass: String,
        gender: String?
    ):Boolean{

        if(userID.isNullOrEmpty() || name.isNullOrEmpty() || email.isNullOrEmpty()
            || age.isNullOrEmpty() || address.isNullOrEmpty() || pass.isNullOrEmpty()
            || gender.isNullOrEmpty()){
            return true
        }
        return  false


    }

    fun validateAge(ageUser:String?):Boolean{

        var age = (ageUser.toString()).toInt()

        if(age in 1..999){
            return true
        }
        return false

    }

    fun validateEmail(email:String?):Boolean{

        if(Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            return true
        }
        return false

    }

    fun validateLength(test:String?):Boolean{

        if(test.toString().length>30){
            return true
        }
        return false

    }

    fun validateID(idNum:String?):Boolean{

        if(idNum.toString().length>10){
            return true
        }
        return false

    }


}